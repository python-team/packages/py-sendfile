py-sendfile (1.2.4-2) UNRELEASED; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Deprecating priority extra as per policy 4.0.1
  * Remove debian/pycompat, it's not used by any modern Python helper
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Jakub Wilk <jwilk@debian.org>  Sun, 05 May 2013 16:01:34 +0200

py-sendfile (1.2.4-1) unstable; urgency=low

  [ Stephan Peijnik ]
  * New upstream release.
  * Switched Vcs-Browser field to viewsvn.
  * debian/control:
    + Updated Homepage field.
  * debian/copyright:
    + Updated upstream author information.
  * debian/watch:
    + Checking for .tar.gz files now, only old releases were uploaded as
      .zip files.

  [ Julien Lavergne ]
  * From Ubuntu, prepare for the future python transition (closes: #556792):
   + debian/rules: Add --install-layout=deb to setup.py install
   + debian/control: Build-depends on python-all-dev (>= 2.5.4-1~)

  [ Sandro Tosi ]
  * debian/control
    - set 'Section: debug' for -dbg package
    - bump Standards-Version to 3.8.3 (no changes needed)

 -- Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>  Fri, 20 Nov 2009 22:23:58 +0100

py-sendfile (1.2.3-1) unstable; urgency=low

  * Initial release (Closes: #515564)

 -- Stephan Peijnik <debian@sp.or.at>  Mon, 09 Feb 2009 09:15:01 +0100
